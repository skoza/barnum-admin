export class User {
    id: string
    custNum: string
    email: string;
    firstName: string;
    lastName: string;
    connection: string;
    loginNum: number;
    latestLogin: number;
    // For Permissions: 0 = none, 1 = customer, 2 = employee, 3 = admin
    barnumCentralAccess: number;
    userAdminAccess: number;
}