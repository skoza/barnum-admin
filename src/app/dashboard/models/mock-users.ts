import { User } from './user';

export const MOCK_USERS: User[] = [
    {
        id: '1',
        custNum: null,
        email: 'tyler@dls0.com',
        firstName: 'Tyler',
        lastName: 'Thompson',
        connection: 'Username-Password-Auth',
        loginNum: 4,
        latestLogin: 22,
        barnumCentralAccess: 3,
        userAdminAccess: 3
    },
    {
        id: '2',
        custNum: null,
        email: 'stevenkoza@gmail.com',
        firstName: 'Steve',
        lastName: 'Koza',
        connection: 'Username-Password-Auth',
        loginNum: 2,
        latestLogin: 5,
        barnumCentralAccess: 3,
        userAdminAccess: 3
    },
    {
        id: '3',
        custNum: null,
        email: 'employee1@hhbarnum.com',
        firstName: 'John',
        lastName: 'Travolta',
        connection: 'Username-Password-Auth',
        loginNum: 6,
        latestLogin: 8,
        barnumCentralAccess: 2,
        userAdminAccess: 0
    },
    {
        id: '4',
        custNum: '1001',
        email: 'customer1@comau.com',
        connection: 'Username-Password-Auth',
        firstName: 'Samuel',
        lastName: 'Jackson',
        loginNum: 1,
        latestLogin: 54,
        barnumCentralAccess: 1,
        userAdminAccess: 0
    },
    {
        id: '5',
        custNum: '2002',
        email: 'customer2@fori.com',
        firstName: 'Tom',
        lastName: 'Cruise',
        connection: 'Username-Password-Auth',
        loginNum: 12,
        latestLogin: 4,
        barnumCentralAccess: 1,
        userAdminAccess: 0
    },
    {
        id: '6',
        custNum: null,
        email: 'admin1@hhbarnum.com',
        firstName: 'Mel',
        lastName: 'Gibson',
        connection: 'Username-Password-Auth',
        loginNum: 14,
        latestLogin: 18,
        barnumCentralAccess: 3,
        userAdminAccess: 3
    },
    {
        id: '7',
        custNum: null,
        email: 'employee2@hhbarnum.com',
        firstName: 'Viggo',
        lastName: 'Mortensen',
        connection: 'Username-Password-Auth',
        loginNum: 0,
        latestLogin: 0,
        barnumCentralAccess: 2,
        userAdminAccess: 0
    },
    {
        id: '8',
        custNum: '3003',
        email: 'customer3@gfs.com',
        firstName: 'Keanu',
        lastName: 'Reeves',
        connection: 'Username-Password-Auth',
        loginNum: 6,
        latestLogin: 4,
        barnumCentralAccess: 1,
        userAdminAccess: 0
    },
    {
        id: '9',
        custNum: '4004',
        email: 'customer4@gfs.com',
        firstName: 'Tom',
        lastName: 'Hanks',
        connection: 'Username-Password-Auth',
        loginNum: 2,
        latestLogin: 2,
        barnumCentralAccess: 1,
        userAdminAccess: 0
    }

]