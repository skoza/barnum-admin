import { Injectable } from '@angular/core';
import { HttpClientModule, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { map, tap, catchError} from 'rxjs/operators';

import { User } from '../models/user';
import { IUser } from '../models/iuser';
import { MOCK_USERS } from '../models/mock-users';
import { useAnimation } from '@angular/animations';

@Injectable({ providedIn: 'root' })

export class UserService {

  apiUrl = 'https://localhost:4000/api/users';
  private token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik0wVTROakk1TmtFd1JVSXhPVEl3TlRrM1FrRTRORGMyT0RjMk5rSTJRamhHTWtWQlJFSTJRUSJ9.eyJpc3MiOiJodHRwczovL2RldmJhcm51bS5hdXRoMC5jb20vIiwic3ViIjoiekw1a1J5TVE3MzZBRTNkcENzVDBRTVpmTU9FdWdxNVFAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vZGV2YmFybnVtLmF1dGgwLmNvbS9hcGkvdjIvIiwiaWF0IjoxNTY3NDMzNjc1LCJleHAiOjE1Njc1MjAwNzUsImF6cCI6InpMNWtSeU1RNzM2QUUzZHBDc1QwUU1aZk1PRXVncTVRIiwic2NvcGUiOiJyZWFkOmNsaWVudF9ncmFudHMgY3JlYXRlOmNsaWVudF9ncmFudHMgZGVsZXRlOmNsaWVudF9ncmFudHMgdXBkYXRlOmNsaWVudF9ncmFudHMgcmVhZDp1c2VycyB1cGRhdGU6dXNlcnMgZGVsZXRlOnVzZXJzIGNyZWF0ZTp1c2VycyByZWFkOnVzZXJzX2FwcF9tZXRhZGF0YSB1cGRhdGU6dXNlcnNfYXBwX21ldGFkYXRhIGRlbGV0ZTp1c2Vyc19hcHBfbWV0YWRhdGEgY3JlYXRlOnVzZXJzX2FwcF9tZXRhZGF0YSBjcmVhdGU6dXNlcl90aWNrZXRzIHJlYWQ6Y2xpZW50cyB1cGRhdGU6Y2xpZW50cyBkZWxldGU6Y2xpZW50cyBjcmVhdGU6Y2xpZW50cyByZWFkOmNsaWVudF9rZXlzIHVwZGF0ZTpjbGllbnRfa2V5cyBkZWxldGU6Y2xpZW50X2tleXMgY3JlYXRlOmNsaWVudF9rZXlzIHJlYWQ6Y29ubmVjdGlvbnMgdXBkYXRlOmNvbm5lY3Rpb25zIGRlbGV0ZTpjb25uZWN0aW9ucyBjcmVhdGU6Y29ubmVjdGlvbnMgcmVhZDpyZXNvdXJjZV9zZXJ2ZXJzIHVwZGF0ZTpyZXNvdXJjZV9zZXJ2ZXJzIGRlbGV0ZTpyZXNvdXJjZV9zZXJ2ZXJzIGNyZWF0ZTpyZXNvdXJjZV9zZXJ2ZXJzIHJlYWQ6ZGV2aWNlX2NyZWRlbnRpYWxzIHVwZGF0ZTpkZXZpY2VfY3JlZGVudGlhbHMgZGVsZXRlOmRldmljZV9jcmVkZW50aWFscyBjcmVhdGU6ZGV2aWNlX2NyZWRlbnRpYWxzIHJlYWQ6cnVsZXMgdXBkYXRlOnJ1bGVzIGRlbGV0ZTpydWxlcyBjcmVhdGU6cnVsZXMgcmVhZDpydWxlc19jb25maWdzIHVwZGF0ZTpydWxlc19jb25maWdzIGRlbGV0ZTpydWxlc19jb25maWdzIHJlYWQ6ZW1haWxfcHJvdmlkZXIgdXBkYXRlOmVtYWlsX3Byb3ZpZGVyIGRlbGV0ZTplbWFpbF9wcm92aWRlciBjcmVhdGU6ZW1haWxfcHJvdmlkZXIgYmxhY2tsaXN0OnRva2VucyByZWFkOnN0YXRzIHJlYWQ6dGVuYW50X3NldHRpbmdzIHVwZGF0ZTp0ZW5hbnRfc2V0dGluZ3MgcmVhZDpsb2dzIHJlYWQ6c2hpZWxkcyBjcmVhdGU6c2hpZWxkcyBkZWxldGU6c2hpZWxkcyByZWFkOmFub21hbHlfYmxvY2tzIGRlbGV0ZTphbm9tYWx5X2Jsb2NrcyB1cGRhdGU6dHJpZ2dlcnMgcmVhZDp0cmlnZ2VycyByZWFkOmdyYW50cyBkZWxldGU6Z3JhbnRzIHJlYWQ6Z3VhcmRpYW5fZmFjdG9ycyB1cGRhdGU6Z3VhcmRpYW5fZmFjdG9ycyByZWFkOmd1YXJkaWFuX2Vucm9sbG1lbnRzIGRlbGV0ZTpndWFyZGlhbl9lbnJvbGxtZW50cyBjcmVhdGU6Z3VhcmRpYW5fZW5yb2xsbWVudF90aWNrZXRzIHJlYWQ6dXNlcl9pZHBfdG9rZW5zIGNyZWF0ZTpwYXNzd29yZHNfY2hlY2tpbmdfam9iIGRlbGV0ZTpwYXNzd29yZHNfY2hlY2tpbmdfam9iIHJlYWQ6Y3VzdG9tX2RvbWFpbnMgZGVsZXRlOmN1c3RvbV9kb21haW5zIGNyZWF0ZTpjdXN0b21fZG9tYWlucyByZWFkOmVtYWlsX3RlbXBsYXRlcyBjcmVhdGU6ZW1haWxfdGVtcGxhdGVzIHVwZGF0ZTplbWFpbF90ZW1wbGF0ZXMgcmVhZDptZmFfcG9saWNpZXMgdXBkYXRlOm1mYV9wb2xpY2llcyByZWFkOnJvbGVzIGNyZWF0ZTpyb2xlcyBkZWxldGU6cm9sZXMgdXBkYXRlOnJvbGVzIHJlYWQ6cHJvbXB0cyB1cGRhdGU6cHJvbXB0cyByZWFkOmJyYW5kaW5nIHVwZGF0ZTpicmFuZGluZyIsImd0eSI6ImNsaWVudC1jcmVkZW50aWFscyJ9.xvJ8LevdcSP6pBXLszMBIJbeyNrRaxX5EzrTdnRnCQUYVkI6wI7Z4VXE08sbKw2QByjeXJpcPLStlkX9khs-IOGN8kIJNp7vDoesBmss1nZObiaE7tTOtVlHj_KnzDlWKqNOg96TbFv0-TUlCeNbIiRLRPTU4LZGml28JufcQoyDxYimyZP4DZ3Y7p_VqgEBU4lWvDuU1Aht5tqKDQXQffKmBhtNIE7bLuotx2wZbNP0jzxldjILspCRrqlktbpblWW2slws6l9Ef9SmypkC_1FXZ_GIbQOf6QPiAIfxArzlFvO8yv91dJZMsHQDl9tc2U1tPHdH1ce1Dbxeh2shyg';

  constructor(private http: HttpClientModule) { }

  // getUsers(): Observable<User[]> {
  //   return
  // }

  getUsers(): Observable<User[]> {
    return of(MOCK_USERS)
    .pipe(catchError(this.handleError)
    );
  }

  updateUser(user: User) {
    console.log("updated user" + user.email);
  }

  addUser(user: User) {
    console.log("added user " + user.email);
    MOCK_USERS.push(user);

  }

  deleteUser(user: User) {
  }

  private handleError(err: HttpErrorResponse | any) {
    console.error('An error occurred', err);
    return throwError(err.message || err);
  }



}

