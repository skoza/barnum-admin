import { Component, OnInit } from '@angular/core';
import Auth0Client from '@auth0/auth0-spa-js/dist/typings/Auth0Client';
import { AuthService } from './../../core/auth/auth.service';

@Component({
  selector: 'dashboard-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class DashboardLayoutComponent implements OnInit {

  isAuthenticated = false;
  profile: any;

  private auth0Client: Auth0Client;

  constructor(public authService: AuthService) { }

  async ngOnInit() {
    // Get instance of Auth0 Client
    this.auth0Client = await this.authService.getAuth0Client();

    // Watch for changes to isAuthenticated state
    this.authService.isAuthenticated.subscribe(value => {
      this.isAuthenticated = value;
    });

    //Watch for changes to profile data
    this.authService.users.subscribe(profile => {
      this.profile = profile;
    });
  }

  // Redirect to Auth0 for login authentication
  async login() {
    await this.auth0Client.loginWithRedirect({
      redirect_uri: `${window.location.origin}/callback`
    });
  }

  // Logs user out of both application and Auth0
  logout() {
    this.auth0Client.logout({
      client_id: this.authService.config.client_id,
      returnTo: window.location.origin
    });
  }

}
