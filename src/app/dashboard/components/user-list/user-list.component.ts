import { User } from '../../models/user';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { UserService } from '../../services/user.service';
import { AuthService } from './../../../core/auth/auth.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogConfig, MatSortable } from '@angular/material';
import { UserDetailsComponent } from './../user-details/user-details.component';

@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  // Auth0
  users: any;

  displayedColumns: string[] = ['email', 'lastName', 'firstName', 'connection', 'loginNum', 'latestLogin'];
  dataSource: MatTableDataSource<User>;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private userService: UserService,
              private dialog: MatDialog,
              private authService: AuthService,
              breakpointObserver: BreakpointObserver) { 
                breakpointObserver.observe(['(max-width: 900px)']).subscribe(result => {
                  this.displayedColumns = result.matches ?
                    ['custNum', 'email', 'connection'] :
                    ['custNum', 'email', 'lastName', 'firstName', 'connection', 'loginNum', 'latestLogin'];
                });
              }

  ngOnInit() {
    this.authService.users.subscribe(users => (this.users = users));
    this.authService.kittens.subscribe(kitten => {
      console.log("steve :" + kitten)
    });
    
    // // TODO: Fix sort triggering before authorization
    this.refreshTable();
    this.sort.sort(({id: 'name', start: 'asc'}) as MatSortable );
  }

  refreshTable() {
    this.getUsers();
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public getUsers() {
    this.userService.getUsers()
      .subscribe(users => this.dataSource = new MatTableDataSource(users));
    }

  filterUsers(value: string) {
    this.dataSource.filter = value.trim().toLowerCase();
  }

  openAddUserDialog(user: User){
    const dialogRef = this.dialog.open(UserDetailsComponent, {
      data: { user : user }
    });
  }

  openEditUserDialog(row: any) {
    var user = new User();
 
    // Sends Row data to Dialog
    if(row){
      user.id = row.id;
      user.custNum = row.custNum;
      user.email = row.email;
      user.firstName = row.firstName;
      user.lastName = row.lastName;
      // TODO: Make radio buttons reflect proper access
      user.barnumCentralAccess = row.barnumCentralAccess;
      user.userAdminAccess = row.userAdminAccess;
    }
   
    const dialogConfig = new MatDialogConfig();
      // TODO: add lose data confirmation modal if forms are filled out
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.data = user;

    const dialogRef = this.dialog.open(UserDetailsComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      this.refreshTable();
    });
  }



  addKitten(name : string) {
    this.authService.makeKitten(name);
  }

  addTyler() {
    this.authService.kittens.subscribe(kitten => {
      console.log('Tyler:' + kitten);
    });
  }
    
}


