import { UserService } from '../../services/user.service';
import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { formControlBinding } from '@angular/forms/src/directives/ng_model';
import { User } from '../../models/user';

@Component({
  selector: 'user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})

export class UserDetailsComponent implements OnInit {

  user: User;

  userForm = new FormGroup({
    custNum: new FormControl(''),
    email: new FormControl('', [Validators.email, Validators.required]),
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    permissions: new FormGroup({
      barnumCentralAccess: new FormControl('none'),
      userAdminAccess: new FormControl('none')
    })
  });

  constructor(private userService: UserService,
    public snackbar: MatSnackBar,
    public dialogRef: MatDialogRef<UserDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) data: any) {
      this.user = data;
    }

  ngOnInit() { 
    if(this.user.id){
      this.userForm.patchValue({
        email: this.user.email,
        firstName: this.user.firstName,
        lastName: this.user.lastName
      });
    }
  }

  save() {
    var user = new User();
    user.email = this.userForm.get('email').value;
    user.firstName = this.userForm.get('firstName').value;
    user.lastName = this.userForm.get('lastName').value;
    // ADD USER
    if(!this.user.id){
      this.userService.addUser(user);
    // EDIT USER
    } else {
      this.userService.updateUser(user);
    }
    this.dialogRef.close();
    this.openConfirmationSnackBar();
  }

  close() {
    this.dialogRef.close('User Not Added!');
  }

  openConfirmationSnackBar() {
    // ADD USER
    if(!this.user.id){
      const snackbarRef = this.snackbar.open('User Added!', 'OK', {
        horizontalPosition:'end'
      });
    // EDIT USER
    } else {
      const snackbarRef = this.snackbar.open('User Updated!', 'OK', {
        horizontalPosition:'end'
      });
    }
  }
  
}
