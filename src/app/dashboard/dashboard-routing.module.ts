import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardAppComponent } from './dashboard-app.component';
import { UserListComponent } from './components/user-list/user-list.component';


const routes: Routes = [
  
  {
    path: '',
    component: DashboardAppComponent,
      children: [
        {
          path: 'list',
          component: UserListComponent
        },
        {
          path: '',
          redirectTo: 'list'
        }
      ]
  },
  {
    path: '**', redirectTo: 'list'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class DashboardRoutingModule { }
