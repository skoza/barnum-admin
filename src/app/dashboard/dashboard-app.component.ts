import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'dashboard-app',
  template: `
    <dashboard-layout></dashboard-layout>
  `,
  styles: []
})
export class DashboardAppComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
