import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardAppComponent } from './dashboard-app.component';
import { DashboardLayoutComponent } from './layout/layout.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { MaterialModule } from './../core/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserService } from './services/user.service';

import { UserListComponent } from './components/user-list/user-list.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';

@NgModule({
  declarations: [
    DashboardAppComponent,
    DashboardLayoutComponent,
    UserListComponent,
    UserDetailsComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  // exports: [
  //   DashboardRoutingModule,
  //   UserListComponent,
  //   EditUserComponent
  // ],
  providers: [
    UserService
  ],
 entryComponents: [
   UserDetailsComponent
  ]
})
export class DashboardModule { }
