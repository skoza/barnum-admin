import { Injectable } from '@angular/core';
import createAuth0Client from '@auth0/auth0-spa-js';
import Auth0Client from '@auth0/auth0-spa-js/dist/typings/Auth0Client';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })

export class AuthService {

  isAuthenticated = new BehaviorSubject(false);
  users = new BehaviorSubject<any>(null);
  kittens = new BehaviorSubject<string>('');

  makeKitten(name : string)  {
    this.kittens.next(name);
  } 

  private auth0Client: Auth0Client;

  //Auth0 app config
  config = {
    domain: "devbarnum.auth0.com",
    client_id: "9kSZo68WY4xLwvWBuB4NQw0QbAlc3lM8",
    redirect_uri: `${window.location.origin}/callback`
  };

  // Gets Auth0Client instance
  async getAuth0Client(): Promise<Auth0Client> {
    if (!this.auth0Client) {
      this.auth0Client = await createAuth0Client(this.config);

      // Provides current value of isAuthenticated
      this.isAuthenticated.next(await this.auth0Client.isAuthenticated());

      // Whenever isAuthenticated changes, provides current value of `getUser`
      this.isAuthenticated.subscribe(async isAuthenticated => {
        if (isAuthenticated) {
          this.users.next(await this.auth0Client.getUser());

          return;
        }
        this.users.next(null);
      });
    }
    return this.auth0Client;
  }

}