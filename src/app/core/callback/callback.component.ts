import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.sass']
})

export class CallbackComponent implements OnInit {

  showSpinner = true;

  constructor(private authService: AuthService,
              private router: Router ) { }

  async ngOnInit() {
    console.log('callback component hit')
    const client = await this.authService.getAuth0Client();
    const result = await client.handleRedirectCallback();

    // Get URL user was trying to reach
    const targetRoute =
      result.appState && result.appState.target ? result.appState.target : '';

    // Update observables
    this.authService.isAuthenticated.next(await client.isAuthenticated());
    this.authService.users.next(await client.getUser());

    // Redirect away
    this.router.navigate([targetRoute]);
  }


}
